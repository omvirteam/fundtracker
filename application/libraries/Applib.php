<?php

/**
 * Class AppLib
 */
class AppLib
{

    function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->database();
    }

    /**
     * @param $tbl_name
     * @param $condition
     * @return mixed
     */
    function counts_rows($tbl_name,$condition = array(1=>1)){
        $this->ci->db->select('1',true);
        $this->ci->db->from($tbl_name);
        $this->ci->db->where($condition);
        $query = $this->ci->db->get();
        return $query->num_rows();
    }

    /**
     * @return mixed
     */
    function getCurrCompetition(){
        $this->ci->db->select('competition_id');
        $this->ci->db->from('competitions');
        $this->ci->db->where('start_datetime <= ',date('Y-m-d H:i:s'));
        $this->ci->db->where('end_datetime >= ',date('Y-m-d H:i:s'));
        $this->ci->db->order_by('competition_id','DESC');
        $this->ci->db->limit('1');
        $query = $this->ci->db->get();
        if($query->num_rows() > 0){
            return $query->row()->competition_id;
        }else{
            $this->ci->session->set_flashdata('success',false);
            $this->ci->session->set_flashdata('message','No any competition currently runnig.');
            redirect('competition/');
        }
    }


    /**
     * @param $competition_id
     * @return mixed
     */
    function getCompetitionDetail($competition_id){
        $this->ci->db->select('*');
        $this->ci->db->from('competitions');
        $this->ci->db->where('competition_id',$competition_id);
        $query = $this->ci->db->get();
        return $query->row();
    }

    /**
     * @param $competition_id
     * @return string
     */
    function getCompetitionStatus($competition_id){
        $this->ci->db->select('start_datetime,end_datetime');
        $this->ci->db->from('competitions');
        $this->ci->db->where('competition_id',$competition_id);
        $this->ci->db->limit('1');
        $query = $this->ci->db->get();
        if($query->row()->start_datetime < date('Y-m-d H:i:s') AND $query->row()->end_datetime < date('Y-m-d H:i:s')){
            return 'completed';
        }elseif($query->row()->start_datetime > date('Y-m-d H:i:s') AND $query->row()->end_datetime > date('Y-m-d H:i:s')){
            return 'pending';
        }else{
            return 'running';
        }
    }

    function getActiveCompetition(){
        $this->ci->db->select('competition_id');
        $this->ci->db->from('competitions');
        $this->ci->db->where('is_active',1);
        $this->ci->db->limit('1');
        $query = $this->ci->db->get();
        return $query->row()->competition_id;
    }

    function getWeeklyHoliday(){
        $this->ci->db->limit('1');
        $query = $this->ci->db->get('weekly_holiday');
        return $query->row()->day;
    }

    function getYearlyHoliday(){
        $this->ci->db->order_by('date');
        $query = $this->ci->db->get('yearly_leaves');
        $YearlyHolidayDates = array();
        foreach($query->result() as $Row){
            $YearlyHolidayDates[] = date('m/d/Y',strtotime(str_replace('-','/',$Row->date)));
        }
        $WeekendHolidayDates = $this->getWeekendHolidayDates();
        foreach($WeekendHolidayDates as $Row){
            $YearlyHolidayDates[] = $Row;
        }
        return $YearlyHolidayDates;
    }

    function getWeekendHolidayDates(){
        $WeeklyHoliday = $this->getWeeklyHoliday();
        $WeekendHolidayDates = array();
        $curr_year = date('Y');
        $start_year = $curr_year - 2;
        $end_year = $curr_year + 2;
        $DatePeriod = new DatePeriod(
            new DateTime("first $WeeklyHoliday of $start_year-01"),
            DateInterval::createFromDateString("next $WeeklyHoliday"),
            new DateTime("last day of $end_year-12")
        );
        foreach ($DatePeriod as $day) {
            if($day->format("Ymd") > date('Ymd')){
                $WeekendHolidayDates[] = $day->format("m/d/Y");
            }
        }
        return $WeekendHolidayDates;

    }

    /***
     * @param $file_url
     * @return bool
     */
    function unlink_file($file_url){
        if(file_exists($file_url)){
            if(unlink($file_url)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * @param $image
     * @param $upload_url
     * @return bool
     */
    function upload_image($image,$upload_url)
    {
        $config['upload_path'] = $upload_url;
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['encrypt_name'] = TRUE;
        $this->ci->load->library('upload', $config);
        $this->ci->upload->initialize($config);
        if (!$this->ci->upload->do_upload($image)) {
            return false;
        }

        $data = $this->ci->upload->data();
        $file_name = $data ['file_name'];
        return $upload_url.$file_name;

    }

    /**
     * @param string $date
     * @return bool|string
     * DD/MM/YYYY To YYYY-MM-DD
     */
    function to_sql_date($date = ''){
        return date('Y-m-d',strtotime($date));
    }

    /**
     * @param string $date
     * @return bool|string
     * YYYY-MM-DD To DD/MM/YYYY
     */
    function to_simple_date($date = ''){
        return date('d/m/Y',strtotime(str_replace('-','/',$date)));
    }
}