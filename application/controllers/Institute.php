<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institute extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('is_logged_in')){
			set_page('dashboard/institute_dashboard');
		}else{
			$this->load->view('auth/login_form');
		}
	}
	public function institute_project_status()
	{
		if($this->session->userdata('is_logged_in')){
			set_page('institute/institute_project_status');
		}else{
			$this->load->view('auth/login_form');
		}
	}
	public function institute_project_status_list()
	{
		if($this->session->userdata('is_logged_in')){
			set_page('institute/institute_project_status_list');
		}else{
			$this->load->view('auth/login_form');
		}
	}
}
