<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Centre extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('is_logged_in')){
			set_page('dashboard/centre_dashboard');
		}else{
			$this->load->view('auth/login_form');
		}
	}
	public function centre_fund_transfer()
	{
		if($this->session->userdata('is_logged_in')){
			set_page('centre/centre_fund_transfer');
		}else{
			$this->load->view('auth/login_form');
		}
	}
	public function centre_fund_transfer_list()
	{
		if($this->session->userdata('is_logged_in')){
			set_page('centre/centre_fund_transfer_list');
		}else{
			$this->load->view('auth/login_form');
		}
	}
}
