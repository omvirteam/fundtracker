<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Users
 */
class Users extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('appmodel');
	}

	function index($user_type = 3)
	{
		set_page('users/list', array('user_type' => $user_type));
	}

	function users_list($user_type = 3)
	{
		set_page('users/list', array('user_type' => $user_type));
	}

	function users_detail($user_id = 'ALL')
	{
		if ($user_id == 'ALL') {
			set_page('voter/list');
		} else {
			$data = $this->appmodel->fetch_user_detail($user_id);
			set_page('users/detail', $data);
		}
	}

	function datatable($user_type = 3)
	{
		$table = 'users';
		if ($user_type == 2) {
			$column_order = array('profile_pic', 'received_votes', 'username', 'email', 'address', 'city', 'user_type', null);
			$column_search = array('profile_pic', 'username', 'email', 'address', 'city');
		} else {
			$column_order = array('profile_pic', 'username', 'email', 'address', 'city', 'user_type', null);
			$column_search = array('profile_pic', 'username', 'email', 'address', 'city');
		}
		$list = $this->appmodel->get_datatables($table, $column_order, $column_search, $user_type);
		$data = array();
		foreach ($list as $user) {
			$row = array();
			$row[] = $user->user_id;
			if($user->profile_pic_type == 2){
				$image_url = $user->profile_pic;
			}else{
				$image_url = $user->profile_pic != '' && $user->profile_pic != null && file_exists(image_dir('profile-pics/thumbs/' . $user->profile_pic)) ? profile_pics_url('thumbs/' . $user->profile_pic) : DEFAULT_IMAGE;
			}
			$row[] = '<a href="javascript:vod(0);"><img src="' . $image_url . '" alt="" class="img-rounded img-preview"></a>';
			if ($user->user_type == '2') {
				$row[] = $user->received_votes;
			}
			$row[] = $user->username;
			$row[] = $user->email;
			$row[] = $user->city;
			$row[] = '<a class="btn btn-sm btn-info" href="' . base_url() . 'users/users_detail/' . $user->user_id . '"  title="Detail"><i class="icon-info22"></i></a> <a class="btn btn-sm btn-danger delete-user" href="javascript:void(0);" title="Delete" data-user_id="' . $user->user_id . '"><i class="icon-trash"></i></a>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->appmodel->count_all($table, $user_type),
			"recordsFiltered" => $this->appmodel->count_filtered($table, $column_order, $column_search, $user_type),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function delete_user($user_id = null)
	{
		if ($user_id != null) {
			$this->db->where('user_id', $user_id);
			$this->db->limit('1');
			$this->db->delete('users');
			if ($this->db->affected_rows() > 0) {
				echo 'success';
			} else {
				echo 'fail';
			}
		} else {
			echo 'fail';
		}
	}
	public function check_email(){
		$email="cshailesh172@gmail.com";  //email to test
		$check = $this->verifyEmail($email,'cshailesh157@gmail.com',true); //your email is just used for smtp requests
		print_r($check);
		/*$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'cshailesh157@gmail.com', // change it to yours
			'smtp_pass' => '17021995Shailesh', // change it to yours
			'mailtype' => 'html',
			'validate' => TRUE,
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);
		$this->load->library('email',$config);
		$this->email->set_newline("\r\n");
		$this->email->from('Photo-Competition','shailesh.chauhan@igts.co.in');
		$this->email->to('cshailesh157@gmail.com');
		$this->email->subject('Testing Email');
		$this->email->message('This is testing email!');
		if ($this->email->send()) {
			echo $this->email->print_debugger();
		} else {
			echo $this->email->print_debugger();
		}*/
	}

	function is_online($server='localhost',$port=80,$timeout=4)   {
		$fp = @fsockopen($server,$port);
		if (!$fp)
			return false;
		else
		{
			return $fp;
		}
	}

	function verifyEmail($toemail, $fromemail, $getdetails = true){
		$result = '';
		$details = '';
		$email_arr = explode("@", $toemail);
		$domain = array_slice($email_arr, -1);
		$domain = $domain[0];
		// Trim [ and ] from beginning and end of domain string, respectively
		$domain = ltrim($domain, "[");
		$domain = rtrim($domain, "]");
		if("IPv6:" == substr($domain, 0, strlen("IPv6:"))) {
			$domain = substr($domain, strlen("IPv6") + 1);
		}
		$mxhosts = array();
		if(filter_var($domain, FILTER_VALIDATE_IP)){
			$mx_ip = $domain;
		} else {
			getmxrr($domain, $mxhosts, $mxweight);
		}
		if(!empty($mxhosts) )
			$mx_ip = $mxhosts[array_search(min($mxweight), $mxhosts)];
		else {
			if( filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ) {
				$record_a = dns_get_record($domain, DNS_A);
			}
			elseif( filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) ) {
				$record_a = dns_get_record($domain, DNS_AAAA);
			}
			if( !empty($record_a) )
				$mx_ip = $record_a[0]['ip'];
			else {
				$result = "invalid";
				$details .= "No suitable MX records found.";
				return ( (true == $getdetails) ? array($result, $details) : $result );
			}
		}
		$connect = @fsockopen($mx_ip,80);
		if($connect){
			if(ereg("^220", $out = fgets($connect, 1024))){
				fputs ($connect , "HELO $HTTP_HOST\r\n");
				$out = fgets ($connect, 1024);
				$details .= $out."\n";

				fputs ($connect , "MAIL FROM: <$fromemail>\r\n");
				$from = fgets ($connect, 1024);
				$details .= $from."\n";
				fputs ($connect , "RCPT TO: <$toemail>\r\n");
				$to = fgets ($connect, 1024);
				$details .= $to."\n";
				fputs ($connect , "QUIT");
				fclose($connect);
				if(!ereg("^250", $from) || !ereg("^250", $to)){
					$result = "invalid";
				}
				else{
					$result = "valid";
				}
			}
		}
		else{
			$result = "invalid";
			$details .= "Could not connect to server";
		}
		if($getdetails){
			return array($result, $details);
		}
		else{
			return $result;
		}
	}
}
