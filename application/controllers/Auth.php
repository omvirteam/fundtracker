<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property AppModel $app_model
 */
class Auth extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Appmodel", "app_model");
    }

    function index()
    {
        if ($this->session->userdata('is_logged_in')) {
            set_page('welcome');
        } else {
            redirect('/auth/login/');
        }
    }

    /**
     * Login user on the site
     *
     * @return void
     */
    function login()
    {
        if ($this->session->userdata('is_logged_in')) {                                    // logged in
            redirect('');
        } else {
            $this->form_validation->set_rules('email','email', 'trim|required|valid_email');
            $this->form_validation->set_rules('pass', 'password', 'trim|required');
            $this->form_validation->set_rules('remember', 'Remember me', 'integer');
            $data['errors'] = array();
            if ($this->form_validation->run()) {
                $email = $_POST['email'];
                $pass = $_POST['pass'];
                $response = $this->app_model->login($email,$pass);
                if ($response) {
                    $this->session->set_userdata('is_logged_in',$response[0]);
                    $this->session->set_flashdata('success',true);
                    $this->session->set_flashdata('message','You have successfully login.');
                    redirect('centre/');
                } else {
                    $data['errors']['email'] = 'Invalid email or password!';
                }
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            $this->load->view('auth/login_form', $data);
        }
    }

    /**
     *
     */
    function logout()
    {
        $this->session->unset_userdata('is_logged_in');
        session_destroy();
        redirect('auth/login');
    }

    /**
     *
     */
    function register()
    {
        if ($this->session->userdata('is_logged_in')) {                                    // logged in
            redirect('');
        } else {
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');
            if ($this->form_validation->run()) {
                $response = $this->api->register($_POST);
                if ($response != false) {
                    if ($response === 'exist_username') {
                        $data['errors']['username'] = 'Username Already Exist.';
                    } elseif ($response === 'exist_email') {
                        $data['errors']['email'] = 'Username Already Exist.';
                    } else {
                        $this->session->set_userdata('is_logged_in', $response);
                        $this->session->set_flashdata('success',true);
                        $this->session->set_flashdata('message','You have successfully register.');
                        redirect('');
                    }
                } else {
                    echo 'false';
                    die;
                }
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            set_page('auth/register_form', $data);
        }
    }

    /**
     *
     */
    function profile()
    {
        if(!$this->session->userdata('is_logged_in')) {                                    // logged in
            redirect('');
        }
        $data = array();
        if (!empty($_POST)) {
            $profile_data = $_POST;
            $this->form_validation->set_rules('name', 'username', 'trim|required|alpha_numeric');
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            $this->form_validation->set_rules('contactno', 'contact no', 'trim|required');
            if ($this->form_validation->run()) {
                if(!empty($_FILES['image']['name'])){
                    $config['upload_path'] = image_dir('staff/');
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('image')){
                        print_r($this->upload->display_errors());
                    }else{
                        $image_data = $this->upload->data();
                        $profile_data['image'] = $image_data['file_name'];
                    }
                }
                $this->db->where('staff_id',$profile_data['staff_id']);
                $this->db->update('user',$profile_data);
                $query = $this->db->get_where('user',array('staff_id',$this->session->userdata('is_logged_in')['staff_id']));
                $response = $query->row_array();
                $this->session->set_userdata('is_logged_in',$response);
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            if(!empty($data)){
                set_page('profile', $data);
            }else{
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','You have successfully save profile.');
                redirect('auth/profile');
            }
        } else {
            $query = $this->db->get_where('user',array('staff_id',$this->session->userdata('is_logged_in')['staff_id']));
            set_page('profile',$query->row());
        }
    }

    function change_password()
    {
        $data = array();
        if (!empty($_POST)) {
            $this->form_validation->set_rules('staff_id', 'User ID', 'trim|required');
            $this->form_validation->set_rules('old_pass', 'old password', 'trim|required|callback_check_old_password');
            $this->form_validation->set_rules('new_pass', 'new password', 'trim|required');
            $this->form_validation->set_rules('confirm_pass', 'confirm Password', 'trim|required|matches[new_pass]');
            if ($this->form_validation->run()) {
                $this->db->where('staff_id',$_POST['staff_id']);
                $this->db->update('user',array('pass'=>md5($_POST['new_pass'])));
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','You have successfully changed password!');
                redirect('auth/profile');
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            set_page('profile',$data);
        } else {
            redirect('auth/profile/');
        }
    }

    function check_old_password($old_pass){
        $staff_id = $_POST['staff_id'];
        $query = $this->db->get_where('user',array('staff_id'=>$staff_id,'pass'=>md5($old_pass)));
        if($query->num_rows() > 0){
            return true;
        }else{
            $this->form_validation->set_message('check_old_password', 'wrong old password.');
            return false;
        }
    }

    function change_profile_pic(){
        if(!empty($_FILES['profile_pic']['name'])){
            $profile_pic = $this->api->upload_image('profile_pic',image_dir('profile-pics/'));
            if ($profile_pic != false) {
                redirect('');
            } else {
                $data['errors']['profile_pic'] = 'Plz select valid image.';
            }
            set_page('profile',$data);
        }else{
            $data['errors']['profile_pic'] = 'Plz select image.';
            set_page('profile',$data);
        }
    }

    function forgot_password(){
        $data = array();
        if(!empty($_POST)){
            $this->form_validation->set_rules('email', 'email', 'trim|required');
            if ($this->form_validation->run()) {
                $response_data = $this->api->forgot_password($_POST);
                if ($response_data == false) {
                    $data['errors']['email'] = 'Incorrect email plz try again.';
                } elseif ($response_data === 'not_send') {
                    $data['errors']['email'] = 'Please try again mail not sanded.';
                }
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            if(!empty($data)){
                set_page('auth/forgot_password_form',$data);
            }else{
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','Plz check inbox.');
                redirect('');
            }
        }else{
            set_page('auth/forgot_password_form');
        }
    }
}
