<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class State extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('is_logged_in')){
			set_page('dashboard/state_dashboard');
		}else{
			$this->load->view('auth/login_form');
		}
	}
	public function state_fund_transfer()
	{
		if($this->session->userdata('is_logged_in')){
			set_page('state/state_fund_transfer');
		}else{
			$this->load->view('auth/login_form');
		}
	}
	public function state_fund_transfer_list()
	{
		if($this->session->userdata('is_logged_in')){
			set_page('state/state_fund_transfer_list');
		}else{
			$this->load->view('auth/login_form');
		}
	}
}
