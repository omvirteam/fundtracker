<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('is_logged_in')){
			set_page('dashboard/vendor_dashboard');
		}else{
			$this->load->view('auth/login_form');
		}
	}
}
