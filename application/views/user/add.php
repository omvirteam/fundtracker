<?php if($this->session->flashdata('success') == true){?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('message')?></h4>
        </div>
    </div>
<?php }?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Add User</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                		<form method="POST" action="<?=base_url('master/save_user') ?>" id="add_user_form">    
	                                        <div class="col-md-12">
	                                            <div class="col-md-6">
	                                                <div class="form-group">
	                                                    <label for="inputEmail3" class="col-sm-3 input-sm">User Type</label>
	                                                    <div class="col-sm-7">
	                                                        <select class="form-control input-sm" name="user_type" id="user_type">
	                                                        	<option>Administrator</option>
	                                                        	<option>Sales Expert</option>
	                                                        	<option>Sales Domestic</option>
	                                                        	<option>Service</option>
	                                                        	<option>Purchase</option>
	                                                        </select>
	                                                    </div>
	                                                </div>
	
	                                                <div class="clearfix"></div>
	
	                                                <div class="form-group">
	                                                    <label for="inputEmail3" class="col-sm-3 input-sm">User Name</label>
	                                                    <div class="col-sm-7">
	                                                        <input type="text" class="form-control input-sm" id="username" name="username">
	                                                    </div>
	                                                </div>
	
	                                                <div class="clearfix"></div>
	
	                                                <div class="form-group">
	                                                    <label for="inputEmail3" class="col-sm-3 input-sm">Phone Number</label>
	                                                    <div class="col-sm-7">
	                                                        <input type="text" class="form-control input-sm" id="phone_number" name="phone_number">
	                                                    </div>
	                                                </div>
	
	                                                <div class="clearfix"></div>
	
	                                                <div class="form-group">
	                                                    <label for="inputEmail3" class="col-sm-3 input-sm">Email</label>
	                                                    <div class="col-sm-7">
	                                                        <input type="text" class="form-control input-sm" id="email" name="email">
	                                                    </div>
	                                                </div>
	
	                                                <div class="clearfix"></div>
	
	                                                <div class="form-group">
	                                                    <label for="inputEmail3" class="col-sm-3 input-sm">Password</label>
	                                                    <div class="col-sm-7">
	                                                        <input type="password" class="form-control input-sm" id="password" name="password">
	                                                    </div>
	                                                </div>
	
	                                                <div class="clearfix"></div>
	
	                                                <div class="form-group">
	                                                    <label for="inputEmail3" class="col-sm-3 input-sm">Confirm Password</label>
	                                                    <div class="col-sm-7">
	                                                        <input type="password" class="form-control input-sm" id="c_password" name="c_password">
	                                                    </div>
	                                                </div>
													
	                                                <div class="form-group">
	                                                    <div class="col-md-5" style="margin-top: 5px;">
	                                                        <button type="submit" class="btn btn-info btn-xs">Submit</button>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
                                        </form>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){

    	$("#add_user_form").on("submit",function(e){
			e.preventDefault();
			var username = $("#user_name").val();
			var phone_number = $("#phone_number").val();
			var email = $("#email").val();
			var password = $("#password").val();
			var c_password = $("#c_password").val();
			if(username != '' && phone_number !='' && email != '' && password != '' && c_password != '')
			{
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					dataType:'json',
					success: function(data){
		                $("#add_user_form")[0].reset();
		                show_notify('Saved Successfully!',true);
					}
				});
			}
		});
		
    });
</script>