<?php if($this->session->flashdata('success') == true){?>
    <div class="col-sm-4 pull-right">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('message')?></h4>
        </div>
    </div>
<?php }?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small class="text-primary text-bold">Users</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                            	<div class="panel panel-default">
													<div class="panel-heading">
														User Detail
														<a href="<?=base_url('master/add_user') ?>" class="btn btn-info btn-xs pull-right">Add User</a>
													</div>
													<div style="margin: 10px;">
						                                 <table id="example1" class="table custom-table">
						                                    <thead>
						                                        <tr>
						                                            <th>Action</th>
						                                            <th>Username</th>
						                                            <th>Email Address</th>
						                                            <th>Phone Number</th>
						                                            <th>User Type</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                                    	<?php 
						                                    	if(!empty($results)) {
						                                    		foreach ($results as $row) {
						                                    	?>
						                                        <tr>
						                                            <td><a href="javascript:void(0);" class="delete_button" data-href="<?=base_url('master/delete/'.$row->id);?>">Delete</a> | <a href="#">Change Password</a> | <a href="#">Edit Profile</a></td>
						                                            <td><?=$row->username ?></td>
						                                            <td><?=$row->email ?></td>
						                                            <td><?=$row->phone_number ?></td>
						                                            <td><?=$row->user_type ?></td>
						                                        </tr>
						                                        <?php 
						                                    		} }
						                                        ?>
						                                    </tbody>
						                                </table>
					                                </div>
				                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $("#example1").DataTable();

        $(document).on("click",".delete_button",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=users',
					success: function(data){
						tr.remove();
						show_notify('Deleted Successfully!',true);
					}
				});
			}
		});
		
    });
</script>