<?php
    $currUrl = $this->uri->segment(1);
?>
<!-- Main sidebar -->
<aside class="main-sidebar" style="overflow-y: auto;height: 100%;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=isset($image) && file_exists(image_dir('staff/'.$image))?image_url('staff/'.$image):dist_url('img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?=$name?$name:'Admin';?></p>
                <a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <?/*=$currUrl == ''?'class="active"':'';*/?>
            <li>
                <a href="<?=base_url();?>centre">
                    <i class="fa fa-bank"></i> <span>Centre</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url();?>state">
                    <i class="fa fa-flag-o"></i> <span>State</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url();?>institute">
                    <i class="fa  fa-graduation-cap"></i> <span>Institute</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url();?>vendor">
                    <i class="fa fa-user"></i> <span>Vendor</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url();?>centre/centre_fund_transfer">
                    <i class="fa fa-dashboard"></i> <span>Add Centre Fund Transfer</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url();?>state/state_fund_transfer">
                    <i class="fa fa-dashboard"></i> <span>Add State Fund Transfer</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url();?>institute/institute_project_status">
                    <i class="fa fa-dashboard"></i> <span>Add Institute Project Status</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url();?>centre/centre_fund_transfer_list">
                    <i class="fa fa-dashboard"></i> <span>Centre Fund Transfer List</span>
                </a>
            </li>
            
            <li>
                <a href="<?=base_url();?>state/state_fund_transfer_list">
                    <i class="fa fa-dashboard"></i> <span>State Fund Transfer List</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url();?>institute/institute_project_status_list">
                    <i class="fa fa-dashboard"></i> <span>Institute Project Status List</span>
                </a>
            </li>
            <?/*=in_array($currUrl,array('party'))?'active':'';*/?>
            <!--<li class="treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-pie-chart"></i> <span>Master</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Party
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url()?>party/"><i class="fa fa-circle-o"></i> Party</a></li>
                            <li><a href="<?=base_url()?>party/country/"><i class="fa fa-circle-o"></i> Country</a></li>
                            <li><a href="<?=base_url()?>party/state/"><i class="fa fa-circle-o"></i> State</a></li>
                            <li><a href="<?=base_url()?>party/city/"><i class="fa fa-circle-o"></i> City</a></li>
                            <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Area</a></li>
                            <li><a href="<?=base_url()?>party/party_type1/"><i class="fa fa-circle-o"></i> Party Type1</a></li>
                            <li><a href="<?=base_url()?>party/party_type2/"><i class="fa fa-circle-o"></i> Party Type2</a></li>
                            <li><a href="<?=base_url()?>party/reference/"><i class="fa fa-circle-o"></i> Reference</a></li>
                            <li><a href="<?=base_url()?>party/designation/"><i class="fa fa-circle-o"></i> Designation</a></li>
                            <li><a href="<?=base_url()?>party/department/"><i class="fa fa-circle-o"></i> Department</a></li>
                            <li><a href="<?=base_url()?>party/terms_group/"><i class="fa fa-circle-o"></i> Terms Group</a></li>
                        </ul>
                    </li>
                    <li><a href="<?=base_url()?>master/company_detail/"><i class="fa fa-circle-o"></i> Company Detail</a></li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Item
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url()?>item/item_master/"><i class="fa fa-circle-o"></i> Item Master</a></li>
                            <li><a href="<?=base_url()?>item/item_category/"><i class="fa fa-circle-o"></i> Item Category</a></li>
                            <li><a href="<?=base_url()?>item/item_group_code/"><i class="fa fa-circle-o"></i> Item Group Code</a></li>
                            <li><a href="<?=base_url()?>item/item_type/"><i class="fa fa-circle-o"></i> Item Type</a></li>
                            <li><a href="<?=base_url()?>item/item_status/"><i class="fa fa-circle-o"></i> Item Status</a></li>
                            <li><a href="<?=base_url()?>item/material_process_type/"><i class="fa fa-circle-o"></i> Material Process Type</a></li>
                            <li><a href="<?=base_url()?>item/main_group/"><i class="fa fa-circle-o"></i> Main Group</a></li>
                            <li><a href="<?=base_url()?>item/sub_group/"><i class="fa fa-circle-o"></i> Sub Group</a></li>
                            <li><a href="<?=base_url()?>item/make/"><i class="fa fa-circle-o"></i> Make</a></li>
                            <li><a href="<?=base_url()?>item/drawing_number/"><i class="fa fa-circle-o"></i> Drawing Number</a></li>
                            <li><a href="<?=base_url()?>item/material_specification/"><i class="fa fa-circle-o"></i> Material Specification</a></li>
                            <li><a href="<?=base_url()?>item/item_class/"><i class="fa fa-circle-o"></i> Class</a></li>
                            <li><a href="<?=base_url()?>item/excise/"><i class="fa fa-circle-o"></i> Excise</a></li>
                        </ul>
                    </li>
                    <li><a href="<?=base_url()?>master/user/"><i class="fa fa-circle-o"></i> User</a></li>
                    
                    <li><a href="<?=base_url()?>master/agent/"><i class="fa fa-circle-o"></i> Agent</a></li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Sales
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url()?>master_sales/reference/"><i class="fa fa-circle-o"></i> Reference</a></li>
                            <li><a href="<?=base_url()?>master_sales/inquiry_stage/"><i class="fa fa-circle-o"></i> Inquiry Stage</a></li>
                            <li><a href="<?=base_url()?>master_sales/industry_type/"><i class="fa fa-circle-o"></i> Industry Type</a></li>
                            <li><a href="<?=base_url()?>master_sales/inquiry_status/"><i class="fa fa-circle-o"></i> Inquiry Status</a></li>
                            <li><a href="<?=base_url()?>master_sales/priority/"><i class="fa fa-circle-o"></i> Priority</a></li>
                            <li><a href="<?=base_url()?>master_sales/lead_provider/"><i class="fa fa-circle-o"></i> Lead Provider</a></li>
							<li><a href="<?=base_url()?>master_sales/uom/"><i class="fa fa-circle-o"></i> UOM</a></li>
                            <li><a href="<?=base_url()?>master_sales/quotation_type/"><i class="fa fa-circle-o"></i> Quotation Type</a></li>
                            <li><a href="<?=base_url()?>master_sales/sales_type/"><i class="fa fa-circle-o"></i> Sales Type</a></li>
                            <li><a href="<?=base_url()?>master_sales/quotation_reason/"><i class="fa fa-circle-o"></i> Quotation Reason</a></li>
                            <li><a href="<?=base_url()?>master_sales/currency/"><i class="fa fa-circle-o"></i> Currency</a></li>
                            <li><a href="<?=base_url()?>master_sales/sales/"><i class="fa fa-circle-o"></i> Sales</a></li>
                            <li><a href="<?=base_url()?>master_sales/action/"><i class="fa fa-circle-o"></i> Action</a></li>
                            <li><a href="<?=base_url()?>master_sales/stage/"><i class="fa fa-circle-o"></i> Stage</a></li>
                            <li><a href="<?=base_url()?>master_sales/status/"><i class="fa fa-circle-o"></i> Status</a></li>
                            <li><a href="<?=base_url()?>master_sales/inquiry_confirmation/"><i class="fa fa-circle-o"></i> Inquiry Confirmation</a></li>
                            <li><a href="<?=base_url()?>master_sales/lead_stage/"><i class="fa fa-circle-o"></i> Lead Stage</a></li>
                            <li><a href="<?=base_url()?>master_sales/lead_source/"><i class="fa fa-circle-o"></i> Lead Source</a></li>
                            <li><a href="<?=base_url()?>master_sales/lead_status/"><i class="fa fa-circle-o"></i> Lead Status</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Terms & Condition
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url()?>terms_condition/terms_condition_group/"><i class="fa fa-circle-o"></i> Terms & Condition Group</a></li>
                            <li><a href="<?=base_url()?>terms_condition/terms_condition_detail/"><i class="fa fa-circle-o"></i> Terms & Condition Details</a></li>
                            <li><a href="<?=base_url()?>terms_condition/default_terms_condition/"><i class="fa fa-circle-o"></i> Default Terms & Condition</a></li>
                            <li><a href="<?=base_url()?>terms_condition/terms_condition_template/"><i class="fa fa-circle-o"></i> Terms & Condition Templates</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> General Master
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url()?>general_master/branch/"><i class="fa fa-circle-o"></i> Branch</a></li>
                            <li><a href="<?=base_url()?>general_master/project/"><i class="fa fa-circle-o"></i> Project</a></li>
                            <li><a href="<?=base_url()?>general_master/billing_template/"><i class="fa fa-circle-o"></i> Billing Template</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-pie-chart"></i>
                    <span>Purchase</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Challan</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Invoice</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Payment</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-pie-chart"></i>
                    <span>Sales</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?=base_url()?>sales/"><i class="fa fa-circle-o"></i> Opportunities (Leads)</a></li>
                    <li><a href="<?=base_url()?>sales/inquiry/"><i class="fa fa-circle-o"></i> Enquiry</a></li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Quotation
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url()?>sales/quotation/"><i class="fa fa-circle-o"></i> New</a></li>
                            <li><a href="<?=base_url()?>sales/quotation_list/"><i class="fa fa-circle-o"></i> Quotation List</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-circle-o"></i>  Sales Order
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url()?>sales/order/"><i class="fa fa-circle-o"></i> New</a></li>
                            <li><a href="<?=base_url()?>sales/order_list/"><i class="fa fa-circle-o"></i> Order List</a></li>
                        </ul>
                    </li>
                   
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-dashboard"></i> <span>Store</span>
                </a>
            </li>
            <li class="treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-pie-chart"></i>
                    <span>Dispatch</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Challan</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-pie-chart"></i>
                    <span>Finance</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Invoice</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Payment</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Proforma Invoice</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Search Invoices</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-dashboard"></i> <span>Reports</span>
                </a>
            </li>
            <li class="treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-pie-chart"></i>
                    <span>General</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Daily Work Entry</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Alerts</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Alerts Type Master</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-pie-chart"></i> <span>HR</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Employee Master
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url()?>hr/employee-master"><i class="fa fa-circle-o"></i> Manage Employee</a></li>
                            <li><a href="<?=base_url()?>hr/list-salary"><i class="fa fa-circle-o"></i> Calculate Salary</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Leave Master
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url()?>hr/total-free-leaves"><i class="fa fa-circle-o"></i> Total Free Leaves</a></li>
                            <li><a href="<?=base_url()?>hr/weekly-leaves"><i class="fa fa-circle-o"></i> Weekly Leaves</a></li>
                            <li><a href="<?=base_url()?>hr/yearly-leaves"><i class="fa fa-circle-o"></i> Yearly Leaves</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?=base_url()?>hr/expected-interview"><i class="fa fa-circle-o"></i> Expected Interview</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>hr/apply-leave"><i class="fa fa-circle-o"></i> Apply For Leave</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-circle-o"></i> Add Letters
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url()?>hr/offer-letter"><i class="fa fa-circle-o"></i> Offer Letter</a></li>
                            <li><a href="<?=base_url()?>hr/appointment-letter"><i class="fa fa-circle-o"></i> Appointment Letter</a></li>
                            <li><a href="<?=base_url()?>hr/confirmation-letter"><i class="fa fa-circle-o"></i> Confirmation Letter</a></li>
                            <li><a href="<?=base_url()?>hr/experience-letter"><i class="fa fa-circle-o"></i> Experience Letter</a></li>
                            <li><a href="<?=base_url()?>hr/noc-letter"><i class="fa fa-circle-o"></i> NOC Letter</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-dashboard"></i> <span>Settings</span>
                </a>
            </li> -->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<!-- /main sidebar -->
