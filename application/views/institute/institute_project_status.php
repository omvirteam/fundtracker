<!-- Main content -->
<div class="content-wrapper">
    <!-- Content area -->
    <div class="content">
		<div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Mumbai University </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?=base_url();?>center_hund_transfer" method="post" enctype="multipart/form-data">
              <div class="box-body">
				<div class="form-group">
                  <label>Vendor</label>
                  <select class="form-control">
                    <option selected>Vendor 1</option>
                    <option>Vendor 2</option>
                    <option>Vendor 3</option>
                    <option>Vendor 4</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Project</label>
                  <select class="form-control">
                    <option selected>Project 1</option>
                    <option>Project 2</option>
                    <option>Project 3</option>
                    <option>Project 4</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <select class="form-control">
                    <option selected>RFP</option>
                    <option>PO</option>
                    <option>Delivery</option>
                    <option>Commission</option>
                    <option>Completed</option>
                  </select>
                </div>
                
                <div class="form-group">
					<label>Date:</label>

					<div class="input-group date">
					  <div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					  </div>
					  <input type="text" class="form-control pull-right" id="datepicker" autocomplete="off">
					</div>
					<!-- /.input group -->
				  </div>
                <div class="form-group">
                  <label for="Fund_Transferred">Fund Transferred</label>
                  <input type="text" class="form-control" id="Fund_Transferred" placeholder="Fund Transferred">
                </div>
                <div class="form-group">
                  <label for="Fund_Transferred">Remarks</label>
                  <textarea id="Remarks" class="form-control" ></textarea>
                </div>
			  </div>
			  <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
    </div>
    <!-- /content area -->
</div>
<!-- /main content -->
