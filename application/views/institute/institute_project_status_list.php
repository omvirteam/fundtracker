<!-- Main content -->
<div class="content-wrapper">
    <!-- Content area -->
    <div class="content">
		<div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Mumbai University Project Status List</h3>
            </div>

			<div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Action</th>
                    <th>Vendor</th>
                    <th>Project</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Fund Transferred</th>
                    <th>Remark</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
					<td>
						<a href="#"><i class="fa fa-fw fa-edit"></i></a>
						<a href="#"><i class="fa fa-fw fa-trash text-danger"></i></a>
					</td>
                    <td>Vendor1</td>
                    <td>Project1</td>
                    <td><span class="label label-success">Delivery</span></td>
                    <td>22/10/2016</td>
                    <td class="text-right">10,00,000</td>
                    <td>Remark</td>
                  </tr>
                  <tr>
					<td>
						<a href="#"><i class="fa fa-fw fa-edit"></i></a>
						<a href="#"><i class="fa fa-fw fa-trash text-danger"></i></a>
					</td>
                    <td>Vendor1</td>
                    <td>Project1</td>
                    <td><span class="label label-warning">Commission</span></td>
                    <td>22/10/2016</td>
                    <td class="text-right">20,50,000</td>
                    <td>Remark</td>
                  </tr>
                  <tr>
					<td>
						<a href="#"><i class="fa fa-fw fa-edit"></i></a>
						<a href="#"><i class="fa fa-fw fa-trash text-danger"></i></a>
					</td>
                    <td>Vendor1</td>
                    <td>Project1</td>
                    <td><span class="label label-danger">PO</span></td>
                    <td>22/10/2016</td>
                    <td class="text-right">3,00,000</td>
                    <td>Remark</td>
                  </tr>
                  <tr>
					<td>
						<a href="#"><i class="fa fa-fw fa-edit"></i></a>
						<a href="#"><i class="fa fa-fw fa-trash text-danger"></i></a>
					</td>
                    <td>Vendor1</td>
                    <td>Project1</td>
                    <td><span class="label label-info">RFP</span></td>
                    <td>22/10/2016</td>
                    <td class="text-right">50,000</td>
                    <td>Remark</td>
                  </tr>
                  <tr>
					<td>
						<a href="#"><i class="fa fa-fw fa-edit"></i></a>
						<a href="#"><i class="fa fa-fw fa-trash text-danger"></i></a>
					</td>
                    <td>Vendor1</td>
                    <td>Project1</td>
                    <td><span class="label label-primary">Completed</span></td>
                    <td>22/10/2016</td>
                    <td class="text-right">5,50,000</td>
                    <td>Remark</td>
                  </tr>
                  
                  </tbody>
                </table>
              </div>
           
          </div>
          <!-- /.box -->

        </div>
    </div>
    <!-- /content area -->
</div>
<!-- /main content -->

