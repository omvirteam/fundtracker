<!-- Main content -->
<div class="content-wrapper">
    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-md-7">
                <form action="<?=base_url();?>auth/profile" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="staff_id" value="<?=$staff_id?>">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title">User Profile</h5>
                        </div>
                        <div class="panel-body">
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label input-sm">Profile Pics</label>
                                <div class="col-sm-9 dispaly-flex">
                                    <input type="file" class="form-control input-sm" id="image" name="image" placeholder="">&nbsp;
                                </div>
                                <?php if(isset($errors['name'])){?><label id="name-error" class="control-label input-sm text-danger" for="name"><?=$errors['name']?></label><?php } ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label input-sm">Name</label>
                                <div class="col-sm-9 dispaly-flex">
                                    <input type="text" class="form-control input-sm" id="name" name="name" value="<?=isset($name)?$name:''?>" placeholder="">&nbsp;
                                </div>
                                <?php if(isset($errors['name'])){?><label id="name-error" class="control-label input-sm text-danger" for="name"><?=$errors['name']?></label><?php } ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label input-sm">Email &nbsp;Address</label>
                                <div class="col-sm-9 dispaly-flex">
                                    <input type="email" class="form-control input-sm" id="email" name="email" value="<?=isset($email)?$email:''?>" placeholder="">&nbsp;
                                </div>
                                <?php if(isset($errors['email'])){?><label id="name-error" class="control-label input-sm text-danger" for="email"><?=$errors['email']?></label><?php } ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label input-sm">Contact no</label>
                                <div class="col-sm-9 dispaly-flex">
                                    <input type="text" class="form-control input-sm" id="contactno" name="contactno" value="<?=isset($contactno)?$contactno:''?>" placeholder="">&nbsp;
                                </div>
                                <?php if(isset($errors['contactno'])){?><label id="name-error" class="control-label input-sm text-danger" for="contactno"><?=$errors['contactno']?></label><?php } ?>
                            </div>
                            <div class="clearfix"></div>
                            <br/>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-info btn-xs pull-right">Save Profiles</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /basic layout -->
            </div>

            <div class="col-md-5">
                <!-- Static mode -->
                <form action="<?=base_url();?>auth/change_password/" method="post">
                    <input type="hidden" name="staff_id" value="<?=isset($staff_id)?$staff_id:'';?>">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title">Change Password</h5>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-4 control-label input-sm">Old Password</label>
                                <div class="col-sm-8 dispaly-flex">
                                    <input type="password" class="form-control input-sm" name="old_pass">
                                </div>
                                <?php if(isset($errors['old_pass'])){?><label id="name-error" class="control-label input-sm text-danger" for="old_pass"><?=$errors['old_pass']?></label><?php } ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label input-sm">New Password</label>
                                <div class="col-sm-8 dispaly-flex">
                                    <input type="password" class="form-control input-sm" name="new_pass">
                                </div>
                                <?php if(isset($errors['new_pass'])){?><label id="name-error" class="control-label input-sm text-danger" for="old_pass"><?=$errors['new_pass']?></label><?php } ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label input-sm">Confirm Password</label>
                                <div class="col-sm-8 dispaly-flex">
                                    <input type="password" class="form-control input-sm" name="confirm_pass">
                                </div>
                                <?php if(isset($errors['confirm_pass'])){?><label id="name-error" class="control-label input-sm text-danger" for="confirm_pass"><?=$errors['confirm_pass']?></label><?php } ?>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-info btn-xs pull-right">Change Password</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /static mode -->
            </div>
        </div>
        <!-- /vertical form options -->
    </div>
    <!-- /content area -->
</div>
<!-- /main content -->