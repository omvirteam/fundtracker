        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>

    <script src="<?=dist_url('js/auto_complete.js');?>"></script>
    <!-- Morris.js charts -->
    <script src="<?=bootstrap_url('js/raphael-min.js');?>"></script>

        <?php if($this->uri->segment(1) == ''){?>
            <script src="<?=plugins_url('morris/morris.min.js');?>"></script>
            <script src="<?=dist_url('js/pages/dashboard.js');?>"></script>
        <?php }?>
    <!-- Sparkline -->
    <script src="<?=plugins_url('sparkline/jquery.sparkline.min.js');?>"></script>
    <!-- jvectormap -->
    <script src="<?=plugins_url('jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
    <script src="<?=plugins_url('jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?=plugins_url('knob/jquery.knob.js');?>"></script>
    <!-- daterangepicker -->
    <script src="<?=bootstrap_url('js/moment.min.js');?>"></script>
    <script src="<?=plugins_url('daterangepicker/daterangepicker.js');?>"></script>
    <!-- datepicker -->
    <script src="<?=plugins_url('datepicker/bootstrap-datepicker.js');?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=plugins_url('bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
    <!-- Slimscroll -->
    <script src="<?=plugins_url('slimScroll/jquery.slimscroll.min.js');?>"></script>

    <!--Page Specific Js-->


        <?php //if($this->uri->segment(2) == 'quotation' || $this->uri->segment(1) == 'party'){?>
            <script src="<?=plugins_url('datatables/jquery.dataTables.min.js');?>"></script>
            <script src="<?=plugins_url('datatables/dataTables.bootstrap.min.js');?>"></script>
        <?php //} ?>

    <script src="<?=plugins_url('iCheck/icheck.min.js');?>"></script>

    <!--Page Specific Js-->

    <!-- FastClick -->
    <script src="<?=plugins_url('fastclick/fastclick.js');?>"></script>
    <!-- AdminLTE App -->
    <script src="<?=dist_url('js/app.min.js');?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=dist_url('js/demo.js');?>"></script>

    <script>
        $(document).ready(function(){
            $(document).on('click','.close',function(){
                console.log('Closed!');
                $(this).closest('div.alert-div').hide();
            });
            $(".input-datepicker").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
            $('input[type="radio"].address_work').iCheck({
                radioClass: 'iradio_minimal-green'
            });
            $('input[type="checkbox"].enquiry_received_by').iCheck({
                checkboxClass: 'icheckbox_flat-green',
            });
            $('#datepicker1,#datepicker2,#datepicker3').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true
            });
        });
    </script>
        <?php
        if($this->uri->segment(1) == 'hr' && ($this->uri->segment(2) == "offer-letter" || $this->uri->segment(2) == "appointment-letter" || $this->uri->segment(2) == "confirmation-letter" || $this->uri->segment(2) == "experience-letter" || $this->uri->segment(2) == "noc-letter")) {
            ?>
            <script src="<?= plugins_url('ckeditor/ckeditor.js'); ?>"></script>
            <script>
                $(function () {
                    CKEDITOR.replace('editor1');
                });
            </script>
            <?php
        }
        ?>

        <?php
            if($this->uri->segment(1) == 'hr' && $this->uri->segment(2) == "yearly-leaves") {
                ?>
                <script src="<?= plugins_url("multi-dates-picker/js/jquery-ui-1.11.1.js"); ?>"></script>
                <script src="<?= plugins_url("multi-dates-picker/jquery-ui.multidatespicker.js"); ?>"></script>
                <script src="<?= plugins_url("multi-dates-picker/js/prettify.js"); ?>"></script>
                <script src="<?= plugins_url("multi-dates-picker/js/lang-css.js"); ?>"></script>
                <script>
                    var today = new Date();
                    var y = today.getFullYear();
                    var Holidays = <?=json_encode($YearlyHoliday)?>;
                    console.log(Holidays);
                    $('#full-year').multiDatesPicker({
                        addDates: Holidays,
                        numberOfMonths: [3, 4],
                        defaultDate: '1/1/' + y
                    });
                </script>
                <!--      Multi Dates Pickers   -->
                <?php
            }
        ?>

        <?php
            if($this->uri->segment(1) == 'hr' && $this->uri->segment(2) == "expected-interview") {
        ?>
            <script>
                $(document).ready(function(){
                    $('#datepicker1,#datepicker2,#datepicker3').datepicker({
                        format: 'dd/mm/yyyy',
                        autoclose: true
                    });
                });
            </script>
        <?php
            }
        ?>
    <!----------------- NOTIFICATION ----------------->

    <script src="<?=plugins_url('notify/jquery.growl.js');?>"></script>
    <script>
        function show_notify(notify_msg,notify_type)
        {
            if(notify_type == true){
                $.growl.notice({ title:"Success!",message:notify_msg});
            }else{
                $.growl.error({ title:"False!",message:notify_msg});
            }
        }
    </script>

    <?php if($this->session->flashdata('success') == 'true'){?>
        <script>
            //show_notify('<?php /* $this->session->flashdata('message') */?>',true);
        </script>
    <?php }?>

    <?php if($this->session->flashdata('success') == 'false'){?>
        <script>
            //show_notify('<?php /* $this->session->flashdata('message') */?>',false);
        </script>
    <?php }?>
    <!----------------- NOTIFICATION ----------------->
    </body>
</html>