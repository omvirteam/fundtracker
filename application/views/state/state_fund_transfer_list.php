<!-- Main content -->
<div class="content-wrapper">
    <!-- Content area -->
    <div class="content">
		<div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Maharashtra State Fund Transfer List</h3>
            </div>

			<div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Action</th>
                    <th>Institute</th>
                    <th>Date</th>
                    <th>Fund Transferred</th>
                    <th>Remark</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
					<td>
						<a href="#"><i class="fa fa-fw fa-edit"></i></a>
						<a href="#"><i class="fa fa-fw fa-trash text-danger"></i></a>
					</td>
                    <td>Mumbai University</td>
                    <td>22/10/20136</td>
                    <td class="text-right">10,00,000</td>
                    <td>Remarks</td>
                  </tr>
                  <tr>
					<td>
						<a href="#"><i class="fa fa-fw fa-edit"></i></a>
						<a href="#"><i class="fa fa-fw fa-trash text-danger"></i></a>
					</td>
                    <td>Pune University</td>
                    <td>22/10/20136</td>
                    <td class="text-right">3,00,000</td>
                    <td>Remarks</td>
                  </tr>
                  <tr>
					<td>
						<a href="#"><i class="fa fa-fw fa-edit"></i></a>
						<a href="#"><i class="fa fa-fw fa-trash text-danger"></i></a>
					</td>
                    <td>Symbiosis University</td>
                    <td>22/10/20136</td>
                    <td class="text-right">50,000</td>
                    <td>Remarks</td>
                  </tr>
                  <tr>
					<td>
						<a href="#"><i class="fa fa-fw fa-edit"></i></a>
						<a href="#"><i class="fa fa-fw fa-trash text-danger"></i></a>
					</td>
                    <td>Sholapur University</td>
                    <td>22/10/20136</td>
                    <td class="text-right">5,50,000</td>
                    <td>Remarks</td>
                  </tr>
                  <tr>
					<td>
						<a href="#"><i class="fa fa-fw fa-edit"></i></a>
						<a href="#"><i class="fa fa-fw fa-trash text-danger"></i></a>
					</td>
                    <td>Mumbai University</td>
                    <td>22/10/20136</td>
                    <td class="text-right">20,50,000</td>
                    <td>Remarks</td>
                  </tr>
                  
                  </tbody>
                </table>
              </div>
           
          </div>
          <!-- /.box -->

        </div>
    </div>
    <!-- /content area -->
</div>
<!-- /main content -->

