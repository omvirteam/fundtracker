<?php 
class Crud extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $table_name
	 * @param $data_array
	 * @return bool
	 */
	function insert($table_name,$data_array){
		if($this->db->insert($table_name,$data_array))
		{
			return $this->db->insert_id();
		}
		return false;
	}

	function getFromSQL($sql){
		return $this->db->query($sql)->result();
	}

	/**
	 * @param $table_name
	 * @param $order_by_column
	 * @param $order_by_value
	 * @return bool
	 */
	function get_all_records($table_name,$order_by_column,$order_by_value){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	/**
	 * @param $table_name
	 * @param $order_by_column
	 * @param $order_by_value
	 * @param $where_array
	 * @return bool
	 */
	function get_all_with_where($table_name,$order_by_column,$order_by_value,$where_array){
		
		// echo "<pre>";print_r($where_array);exit;

		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_array);
		$this->db->order_by($order_by_column,$order_by_value);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	/**
	 * @param $tbl_name
	 * @param $column_name
	 * @param $where_id
	 * @return mixed
	 */
	function get_column_value_by_id($tbl_name,$column_name,$where_id)
	{				
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where_id);		
		$this->db->last_query();
		$query = $this->db->get();
		return $query->row($column_name);
	}

	/**
	 * @param $table_name
	 * @param $where_id
	 * @return mixed
	 */
	function get_row_by_id($table_name,$where_id){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_id);
		$query = $this->db->get();
		return $query->result();
	}

	/**
	 * @param $table_name
	 * @param $where_array
	 * @return mixed
	 */
	function delete($table_name,$where_array){		
		$result = $this->db->delete($table_name,$where_array);
		return $result;
	}

	/**
	 * @param $table_name
	 * @param $data_array
	 * @param $where_array
	 * @return mixed
	 */
	function update($table_name,$data_array,$where_array){
		$this->db->where($where_array);
		$rs = $this->db->update($table_name, $data_array);
		return $rs;
	}

	/**
	 * @return string
	 */
	function get_state()
	{
		if(isset($_POST['country_id'])){
		
			$countryID =  $_POST['country_id'];
			$where = array('country_id'=>$countryID);
			$result = $this->get_all_with_where('state','state_id','ASC',$where);
			$option = '<option value="">Select State</option>';
		
			if($result)
			{
				foreach ($result as $row){
					$state_id = $row->state_id;
					$state = $row->state;
					$option .= '<option value="'.$state_id.'">'.$state.'</option>';
				}
			}
			return $option;
		}
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	function search_leads($data){
			
			$lead_source = $this->get_id_by_val('lead_source','id','lead_source',$data['lead_source']);
			$lead_stage = $this->get_id_by_val('lead_stage','id','lead_stage',$data['lead_stage']);
			$industry_type = $this->get_id_by_val('industry_type','id','industry_type',$data['industry_type']);
			$lead_owner = $this->get_id_by_val('staff','staff_id','name',$data['lead_owner']);
			$assigned_to = $this->get_id_by_val('staff','staff_id','name',$data['assigned_to']);
			$created_by = $data['created_by'];
			$this->db->select("*");
			$this->db->from("lead_details");
			
			if($data['lead_source']!="" && $lead_source !=''){
				$this->db->where("lead_source",$lead_source);
			}
				
			if($data['lead_stage']!=""  && $lead_stage !=''){
				$this->db->where("lead_stage",$lead_stage);
			}
				
			if($data['industry_type']!="" && $industry_type != ''){
				$this->db->where("industry_type",$industry_type);
			}
				
			if($data['lead_owner']!="" && $lead_owner != ''){
				$this->db->where("lead_owner",$lead_owner);
			}
				
			if($data['assigned_to']!="" && $assigned_to != ''){
				$this->db->where("assigned_to",$assigned_to);
			}

			if($created_by != 'all'){
				$this->db->where("created_by",$created_by);
			}
				
			$query = $this->db->get();
			return $result = $query->result();
			
	}

	/**
	 * @param $name
	 * @param $path
	 * @return bool
	 */
	function upload_file($name, $path)
	{
		$config['upload_path'] = $path;
		$config ['allowed_types'] = '*';
		$this->upload->initialize($config);
		if($this->upload->do_upload($name))
		{
			$upload_data = $this->upload->data();
			return $upload_data['file_name'];
		}
		return false;
	}

	/**
	 * @param $table
	 * @param $column
	 * @param $search_value
	 * @return mixed
	 */
	function getAutoCompleteData($table,$column,$search_value){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->like($column,$search_value);
		$query = $this->db->get();
		return $query->result();
	}

	/**
	 * @param $table
	 * @param $id_column
	 * @param $column
	 * @param $column_val
	 * @return null
	 */
	function get_id_by_val($table,$id_column,$column,$column_val){
		$this->db->select($id_column);
		$this->db->from($table);
		$this->db->where($column,$column_val);
		$this->db->limit('1');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->$id_column;
		} else {
			return null;
		}
	}
	
	function get_column_value_by_quotation($where_id)
	{		
		$this->db->select("*");
		$this->db->from('quotations');
		$this->db->where('id',$where_id);		
		$query = $this->db->get();
		return $query->row();
	}
	
	function get_items_detail_by_quotation($where_id)
	{
		$this->db->select("quotation_items.id as quotation_id ,quotation_items.item_code,quotation_items.item_description,quotation_items.uom_id,quotation_items.quantity,quotation_items.rate,quotation_items.amount,quotation_items.amount,item_status.item_status as status");
		$this->db->from('quotation_items');
		$this->db->join('item_status', 'quotation_items.item_status_id = item_status.id', 'left');
		$this->db->where('quotation_id',$where_id);		
		$query = $this->db->get();
		return $query->result();
	}
	function get_selected_items_detail_by_quotation($where_id)
	{
		$this->db->select("*");
		$this->db->from('quotation_items');
		$this->db->where('id',$where_id);		
		$query = $this->db->get();
		return $query->row();
	}
}
