-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 21, 2016 at 02:18 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fundtracking`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `staff_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `bdate` date NOT NULL,
  `marriagedate` date NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `mothername` varchar(255) NOT NULL,
  `maritalstatus` varchar(15) NOT NULL,
  `husband_wifename` varchar(255) NOT NULL,
  `bloodgroup` varchar(10) NOT NULL,
  `contactno` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `designation` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `grade` varchar(30) NOT NULL,
  `doj` datetime NOT NULL,
  `dol` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`staff_id`, `name`, `email`, `pass`, `image`, `bdate`, `marriagedate`, `qualification`, `gender`, `middlename`, `lastname`, `mothername`, `maritalstatus`, `husband_wifename`, `bloodgroup`, `contactno`, `address`, `designation`, `department`, `grade`, `doj`, `dol`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'admin', 'admin@gmail.com', '75d23af433e0cea4c0e45a56dba18b30', 'd2fcc01ce814a214f91229e76022ddd2.png', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '4234234', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-10-21 11:11:52', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`staff_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
